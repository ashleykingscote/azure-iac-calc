import requests
import os
import sys
from pprint import pprint
from readFromStorage import readTFState
import argparse
import json
import logging

azureResources = []


def getToken(url="https://graph.microsoft.com"):
    data = "grant_type=client_credentials&\
            client_id={}&\
            client_secret={}&\
            resource={}".format(ARM_CLIENT_ID, ARM_CLIENT_SECRET, url)
    response = requests.post("https://login.microsoftonline.com/{}/oauth2/token".format(ARM_TENANT_ID), data=data)
    if response.status_code == 200:
        return response.json()["access_token"]
    else:
        logger.error("Error getting token - {}".format(response.status_code))
        logger.debug(response.json())
        sys.exit()


def getResources(url, token):
    headers = {
        "Authorization": "Bearer {}".format(token),
        "Content-Type": "application/json"
    }

    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        return response.json()["value"]
    else:
        logger.error("Error getting resources for url {}".format(url))
        sys.exit()


def processResources(resources):
    for resource in resources:
        azureResources.append(resource["id"])


def getResourcesFromAzure():
    # get azure resources for each subscription
    for sub in subscriptions:
        token = getToken("https://management.core.windows.net/")
        url = "https://management.azure.com/subscriptions/{}/resources?api-version=2020-06-01".format(sub)
        data = getResources(url, token)
        processResources(data)

    # get azure applications
    token = getToken()
    url = "https://graph.microsoft.com/v1.0/applications"
    data = getResources(url, token)
    processResources(data)

    # get azure groups
    token = getToken()
    url = "https://graph.microsoft.com/v1.0/groups"
    data = getResources(url, token)
    processResources(data)


def percentageCalc(azureResources, notInTerraform):
    totalAzure = len(azureResources)
    logger.info("{} resources in azure".format(totalAzure))

    totalNotInTerraform = len(notInTerraform)
    logger.info("{} resources not in terraform".format(totalNotInTerraform))

    percentage = 100 - (totalNotInTerraform/totalAzure * 100)
    return percentage


def envCheck():
    envs = ["ARM_CLIENT_ID", "ARM_CLIENT_SECRET", "ARM_TENANT_ID", "ARM_SUBSCRIPTION_ID"]
    for env in envs:
        if os.environ.get(env) is None:
            logger.error("Please set OS Environment variable {}".format(env))
            sys.exit()


if "__main__" == __name__:
    parser = argparse.ArgumentParser()
    parser.add_argument("-l", "--log", dest="logLevel", default="INFO", choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'], help="Set the logging level")

    args = parser.parse_args()

    # Create a custom logger
    logger = logging.getLogger(__name__)
    handler = logging.StreamHandler()
    handler.setFormatter(logging.Formatter('%(levelname)s - %(message)s'))
    logger.setLevel(getattr(logging, args.logLevel))
    logger.addHandler(handler)

    with open('config.json') as json_file:
        config = json.load(json_file)

    storage_account = config["storage_account"]
    storage_account_rg = config["storage_account_rg"]
    subscriptions = config["subscriptions"]

    envCheck()

    ARM_CLIENT_ID = os.environ["ARM_CLIENT_ID"]
    ARM_CLIENT_SECRET = os.environ["ARM_CLIENT_SECRET"]
    ARM_TENANT_ID = os.environ["ARM_TENANT_ID"]

    getResourcesFromAzure()

    ids = readTFState(storage_account_rg, storage_account)

    notManagedByTerraform = []
    for azresource in azureResources:
        if azresource not in ids:
            notManagedByTerraform.append(azresource)
        else:
            logger.debug("{} is present in terraform state".format(azresource))

    logger.info("{:.2f}% managed by terraform".format(percentageCalc(azureResources, notManagedByTerraform)))
