from azure.storage.blob import BlobServiceClient
from io import BytesIO
from pprint import pprint
import json

import os
from azure.mgmt.storage import StorageManagementClient
from azure.mgmt.storage.models import StorageAccountCreateParameters

from azure.identity import ClientSecretCredential


def getStorageKey(storage_account_rg, storage_account):
    credential = ClientSecretCredential(
        tenant_id=os.environ['ARM_TENANT_ID'],
        client_id=os.environ['ARM_CLIENT_ID'],
        client_secret=os.environ['ARM_CLIENT_SECRET']
    )
    storage_client = StorageManagementClient(credential,  os.environ.get('ARM_SUBSCRIPTION_ID'))
    storage_keys = storage_client.storage_accounts.list_keys(storage_account_rg, storage_account)
    storage_keys = {v.key_name: v.value for v in storage_keys.keys}
    return storage_keys['key1']


def readTFState(storage_account_rg, storage_account):
    key1 = getStorageKey(storage_account_rg, storage_account)
    ids = []

    # list all containers
    blob_service = BlobServiceClient(account_url="https://{}.blob.core.windows.net".format(storage_account), credential=key1)
    containers = blob_service.list_containers()
    state_container = None
    for c in containers:
        if "state" in c.name:
            state_container = c.name
    if not state_container:
        print("Did not find state container")
        return

    blob_service_client = BlobServiceClient(account_url="https://sdscoredev6202.blob.core.windows.net", credential=key1)
    container_client = blob_service_client.get_container_client(state_container)
    for blob in container_client.list_blobs():
        # Download a blob example
        # if blob.name == "core/rg-core/terraform.tfstate":
        #     blob_client = blob_service_client.get_blob_client(container=state_container, blob=blob.name)
        #     streamdownloader=blob_client.download_blob()
        #     stream = BytesIO()
        #     pprint(streamdownloader.download_to_stream(stream))

        #################################
        blob_client = blob_service_client.get_blob_client(container="terraform-dev-state-c877", blob=blob.name)
        contents_raw = blob_client.download_blob().readall()
        # convert to dict
        content = json.loads(contents_raw.decode())
        try:
            id = content["outputs"]["id"]["value"]
            ids.append(id)
        except KeyError:
            # pprint(content)
            pass
    return ids
