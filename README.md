## Azure Terraform Managed Resources

This application compares resources from Azure against a terraform statefile in an Azure storage account and derives a rough percentage of how much deployed infrastructure is provisioned with terraform.

Firstly, all resources from the provided subscriptions are gathered and the `id` values are added to an array.
Next, all applications (service principals) are retrieved for the Azure tenant and the `id` values are added to an array.
Finally, all azure groups are retrieved for the tenant and the `id` values are also added to the array.


The storage account key is retrieved and and the a container containing `state` in the name is retrieved.
All blobs within the container are processed (all sub directories) and if there is an `id` value in the output of the terraform status, the value is added to a seperate `terraform` array.

So now we have two arrays of `ids`. They wont all be in the same format, some id values are URLs to the resource, whereas some ID values are UUID's.

Finally, the `azure` array is iterated. If the azure array id value is **not** present within the terraform array, it is added to an `unmanaged` array.
The length of the `umanaged` array is compared to the azure resources array to derive a managed percentage.

Loosely styled with `pep8` using `pycodestyle`
```
python3 -m pycodestyle iacPercentage.py 
python3 -m pycodestyle readFromStorage.py 
```

## ToDos
- Get resource group automatically from the storage account name, or get the storage account from the resource group name. Either way, reduce the number of required config items.
- Add logging to `readFromStorage.py`
- Automatically get the subscriptions for the app registration, rather than manually specify them

## Package Installation
The following packages need to be installd.

Note - i couldnt install `azure-mgmt-storage>=3.0.0` with `zsh`, i had to use `bash`.

```
pip3 install azure-storage-blob azure-mgmt-storage>=3.0.0 msrestazure azure-identity
```

## Execution
The application expects a `config.json` file with the following contents:
```
{
    "storage_account" : "xxxxxxxxxx",
    "storage_account_rg" : "xxxxx",
    "subscriptions" : [
        "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
        "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
    ]
}
```
The storage account name and the storage account resource group name
The subscriptions where your resources reside.

```
python3 iacPercentage.py
...
INFO - 180 resources in azure
INFO - 118 resources not in terraform
INFO - 34.44% managed by terraform
```

With debug
```
python3 iacPercentage.py -l DEBUG
...
DEBUG - /subscriptions/xxxxxxxxxxxxxxxxxxxxxxx/resourceGroups/xxxxxxxx/providers/Microsoft.Network/virtualNetworks/xxxxxxxx is present in terraform state
DEBUG - xxxxxxxx-xxxx-xxxxxxxxx-xxxxxxxxxxxx is present in terraform state
DEBUG - xxxxxxxx-xxxx-xxxxxxxxx-xxxxxxxxxxxx is present in terraform state
DEBUG - xxxxxxxx-xxxx-xxxxxxxxx-xxxxxxxxxxxx is present in terraform state
DEBUG - xxxxxxxx-xxxx-xxxxxxxxx-xxxxxxxxxxxx is present in terraform state
DEBUG - xxxxxxxx-xxxx-xxxxxxxxx-xxxxxxxxxxxx is present in terraform state
DEBUG - xxxxxxxx-xxxx-xxxxxxxxx-xxxxxxxxxxxx is present in terraform state
INFO - 180 resources in azure
INFO - 118 resources not in terraform
INFO - 34.44% managed by terraform
```